package com.appdonatortestcase.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.appdonatortestcase.app.R;

/**
 * Created by Artacho on 11/06/14.
 */
public class MessageDialogFragment extends DialogFragment {

    private static final String KEY_TITLE = "title";

    private String mAlertTitle;
    private String mAlertContent;

    public MessageDialogFragment() {
        super();
    }

    public MessageDialogFragment(String mCallTitle, String mCallContent) {
        mAlertTitle = mCallTitle;
        mAlertContent = mCallContent;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mAlertTitle = savedInstanceState.getString(KEY_TITLE);
        }
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(mAlertTitle);
        builder.setMessage(mAlertContent)
                .setPositiveButton(R.string.send_option, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked SEND Button
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setType("vnd.android-dir/mms-sms");
                        smsIntent.putExtra("address", getString(R.string.phone));
                        smsIntent.putExtra("sms_body",getString(R.string.sms_key));
                        startActivity(smsIntent);
                    }
                })
                .setNegativeButton(R.string.cancel_option,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                // User cancelled the dialog
                            }
                        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_TITLE, mAlertTitle);
    }
}