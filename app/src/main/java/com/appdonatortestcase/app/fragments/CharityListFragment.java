package com.appdonatortestcase.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.appdonatortestcase.app.R;
import com.appdonatortestcase.app.activities.DonationSettingsActivity;

/**
 * Created by Artacho on 10/06/14.
 */
public class CharityListFragment extends Fragment {

    private String[] mTitles;
    private ListView mCharityListView;
    private ArrayAdapter<String> mArrayAdapter;

    public static final String EXTRA_POSITION = "extra_list_position";

    private static final String TAG = "CharityListFragment";

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_charity_list,
                container, false);

        mCharityListView = (ListView)view.findViewById(R.id.mainListView);
        mTitles = getResources().getStringArray(R.array.organizations_array);
        mArrayAdapter = new MyArrayAdapter<String>(getActivity(), mTitles);

        mCharityListView.setAdapter(mArrayAdapter);

        mCharityListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent donationSettingsActivity = new Intent(getActivity(), DonationSettingsActivity.class);
                donationSettingsActivity.putExtra(DonationSettingsFragment.EXTRA_POSITION, position);
                startActivity(donationSettingsActivity);

            }
        });

                return view;
    }

    public class MyArrayAdapter<T> extends ArrayAdapter<T> {

        public MyArrayAdapter(Context context, T[] items) {

            super(context, R.layout.item_charity_list, R.id.organization_title, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = super.getView(position, convertView, parent);
            TextView textView = (TextView) view.findViewById(R.id.organization_title);
            view.setBackgroundResource(R.color.base_selector);

            return view;
        }
    }
}
