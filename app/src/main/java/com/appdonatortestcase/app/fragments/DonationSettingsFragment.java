package com.appdonatortestcase.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appdonatortestcase.app.R;
import com.appdonatortestcase.app.dialogs.CallDialogFragment;
import com.appdonatortestcase.app.dialogs.MessageDialogFragment;

import org.w3c.dom.Text;

/**
 * Created by Artacho on 10/06/14.
 */
public class DonationSettingsFragment extends Fragment {

    private String[] mTitles;
    private int mRadioValue = 0;
    private String mDialogTitle;
    private String mDialogContent;

    private TextView mOrganizationName;
    private RadioGroup mRadioGroup;
    private RadioGroup mRadioGroupMethod;
    private TextView mSumTextView;
    private TextView mFrequency;
    private EditText mQuantity;
    private TextView mTextExplanation;
    private Button mDonateButton;


    public static final String EXTRA_POSITION = "extra_list_position";

    private static final String TAG = "DonationSettingsFragment";

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_donation_settings,
                container, false);

        mRadioGroup = (RadioGroup)view.findViewById(R.id.radio_group);
        mRadioGroupMethod = (RadioGroup)view.findViewById(R.id.method_group);

        mOrganizationName = (TextView) view.findViewById(R.id.organization_name);
        mTextExplanation = (TextView) view.findViewById(R.id.text_view_explanation);
        mSumTextView = (TextView) view.findViewById(R.id.sum_text_view);
        mFrequency = (TextView) view.findViewById(R.id.frequency_text_view);

        mQuantity = (EditText) view.findViewById(R.id.quantity_edit_text);
        mDonateButton = (Button) view.findViewById(R.id.donate_button);

        mTextExplanation.setText(getText(R.string.card_text_view));
        mTitles = getResources().getStringArray(R.array.organizations_array);

        Intent intent = getActivity().getIntent();
        int position = intent.getIntExtra(CharityListFragment.EXTRA_POSITION, -1);
        mOrganizationName.setText(mTitles[position]);

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio_credit_card) {
                    mRadioValue = 1;
                    mFrequency.setVisibility(View.VISIBLE);
                    mRadioGroupMethod.setVisibility(View.VISIBLE);
                    mSumTextView.setVisibility(View.VISIBLE);
                    mQuantity.setVisibility(View.VISIBLE);
                    mTextExplanation.setText(getText(R.string.card_text_view));

                }  else if (checkedId == R.id.radio_sms) {
                    mRadioValue = 2;
                    mFrequency.setVisibility(View.GONE);
                    mRadioGroupMethod.setVisibility(View.GONE);
                    mSumTextView.setVisibility(View.GONE);
                    mQuantity.setVisibility(View.GONE);
                    mTextExplanation.setText(getText(R.string.sms_text_view));
                } else {
                    mRadioValue = 3;
                    mFrequency.setVisibility(View.GONE);
                    mRadioGroupMethod.setVisibility(View.GONE);
                    mSumTextView.setVisibility(View.GONE);
                    mQuantity.setVisibility(View.GONE);
                    mTextExplanation.setText(getText(R.string.call_text_view));
                }
            }
        });

        mDonateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mRadioValue) {
                    case 1:
                        //Credit Card
                        Toast toastCreditCard =
                                Toast.makeText(getActivity(),
                                        getString(R.string.thanks), Toast.LENGTH_SHORT);
                        toastCreditCard.show();
                        break;
                    case 2:
                        //SMS Payment
                        mDialogTitle = getString(R.string.dialog_title);
                        mDialogContent = getString(R.string.sms_content);

                        MessageDialogFragment messageDialogFragment
                                = new MessageDialogFragment(mDialogTitle, mDialogContent);
                        messageDialogFragment.show(getFragmentManager(), TAG);

                        break;

                    case 3:
                        //Premum Voice Call
                        mDialogTitle = getString(R.string.dialog_title);
                        mDialogContent = getString(R.string.call_content);

                        CallDialogFragment callDialogFragment
                                = new CallDialogFragment(mDialogTitle, mDialogContent);
                        callDialogFragment.show(getFragmentManager(), TAG);
                        break;

                    default:
                        break;
                }
            }
        });
            return view;
        }
    }
