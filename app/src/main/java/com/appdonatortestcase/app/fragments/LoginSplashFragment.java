package com.appdonatortestcase.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appdonatortestcase.app.R;

/**
 * Created by Artacho on 10/06/14.
 */
public class LoginSplashFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_splash,
                container, false);
        return view;
    }
}
